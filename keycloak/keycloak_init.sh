#!/bin/bash
#
# This script will launch and configure Keycloak with a NGINX reverse proxy for SSL
# SSL certifiates are generated using certbot against previously created Step CA Acme server
# 
# 

set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd -P)

usage() {
  cat<<EOF
Usage: $(basename "${BASH_SOURCE[0]}")

Keycloak initialization script

Available options:

-h, --help          Print this help and exit
-v, --verbose       Print script debug info
-c, --ca            CA server
-d, --domain        Server Domain Name
--step-cli          Step CLI version
-f, --fingerprint   CA Fingerprint
-s, --db-secret     Postgress DB
-r, --realm         Keycloak Realm
-u, --user          Keycloak Admin User
-p, --pass          Keycloak Admin pass
EOF
  exit
}

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  # script cleanup here
}

setup_colors() {
  if [[ -t 2 ]] && [[ -z "${NO_COLOR-}" ]] && [[ "${TERM-}" != "dumb" ]]; then
    NOFORMAT='\033[0m' RED='\033[0;31m' GREEN='\033[0;32m' ORANGE='\033[0;33m' BLUE='\033[0;34m' PURPLE='\033[0;35m' CYAN='\033[0;36m' YELLOW='\033[1;33m'
  else
    NOFORMAT='' RED='' GREEN='' ORANGE='' BLUE='' PURPLE='' CYAN='' YELLOW=''
  fi
}

msg() {
  echo >&2 -e "${1-}"
}

msg_dbg() {
  if [[ "$verbose" == 1 ]];
  then
    echo >&2 -e "${1-}"
  fi
}


parse_params() {
  # default values of variables set from params
  KEYCLOAK_DOMAIN=""
  KEYCLOAK_CA_SERVER=""
  KEYCLOAK_CA_FINGERPINT=""
  STEP_CLI_VERSION=""
  KEYCLOAK_DB_PASS=""
  KEYCLOAK_REALM=""
  verbose=0
  while :; do
    case "${1-}" in
    -h | --help) usage ;;
    -v | --verbose) verbose=1 ;;
    -d | --domain)
      KEYCLOAK_DOMAIN="${2-}"
      shift
      ;;
    -c | --ca)
      KEYCLOAK_CA_SERVER="${2-}"
      shift
      ;;
    -f | --fingerprint)
      KEYCLOAK_CA_FINGERPINT="${2-}"
      shift
      ;;
    --step-cli)
      STEP_CLI_VERSION="${2-}"
      shift
      ;;
    -s | --db-secret)
      KEYCLOAK_DB_PASS="${2-}"
      shift
      ;;
    -r | --realm)
      KEYCLOAK_REALM="${2-}"
      shift
      ;;
    -u | --user)
      KEYCLOAK_USER="${2-}"
      shift
      ;;
    -p | --pass)
      KEYCLOAK_PASS="${2-}"
      shift
      ;;
    -?*) die "Unknown option: $1" ;;
    *) break ;;
    esac
    shift
  done

  args=("$@")

  # check required params and arguments
  #[[ -z "${param-}" ]] && die "Missing required parameter: param"
  #[[ ${#args[@]} -eq 0 ]] && die "Missing script arguments"

  return 0
}


parse_params "$@"

msg "Configuring Parameters"
if [[ -z "$KEYCLOAK_DOMAIN" ]];
then
  msg "Keycloak Domain name: "
  read KEYCLOAK_DOMAIN
fi
if [[ -z "$KEYCLOAK_CA_SERVER" ]];
then
  msg "CA Server: "
  read KEYCLOAK_CA_SERVER
fi
if [[ -z "$KEYCLOAK_CA_FINGERPINT" ]];
then
  msg "Step CA Fingerprint: "
  read KEYCLOAK_CA_FINGERPINT
fi
if [[ -z "$KEYCLOAK_REALM" ]];
then
  msg "Keycloak Realm Name: "
  read KEYCLOAK_REALM
fi
if [[ -z "$KEYCLOAK_DB_PASS" ]];
then
  msg "Enter Keycloak DB Password: "
  read -s KEYCLOAK_DB_PASS
  msg "Retype Keycloak DB Password: "
  read -s KEYCLOAK_DB_PASS2
  msg ""
  while [ $KEYCLOAK_DB_PASS != $KEYCLOAK_DB_PASS2 ];
  do
    msg "Keycloak DB Password does not match"
    msg "Enter Keycloak DB Password: "
    read -s KEYCLOAK_DB_PASS
    msg "Retype Keycloak DB Password: "
    read -s KEYCLOAK_DB_PASS2
    msg ""
  done
fi
if [[ -z "$KEYCLOAK_USER" ]];
then
  msg "Keycloak User: "
  read KEYCLOAK_USER
fi
if [[ -z "$KEYCLOAK_PASS" ]];
then
  msg "Enter KeycloakPassword: "
  read -s KEYCLOAK_PASS
  msg "Retype Keycloak DB Password: "
  read -s KEYCLOAK_PASS2
  msg ""
  while [ $KEYCLOAK_PASS != $KEYCLOAK_PASS2 ];
  do
    msg "Keycloak Password does not match"
    msg "Enter Keycloak Password: "
    read -s KEYCLOAK_PASS
    msg "Retype Keycloak Password: "
    read -s KEYCLOAK_PASS2
    msg ""
  done
fi

if [[ -z "$STEP_CLI_VERSION" ]]
then
    STEPCLI_DOWNLOAD=$(curl -s https://api.github.com/repos/smallstep/cli/releases/latest | grep "browser_download_url.*deb" | cut -d : -f 2,3 | tr -d \")
else
    STEPCLI_DOWNLOAD="https://github.com/smallstep/cli/releases/download/v${STEPCLI_VERSION}/step-cli_${STEPCLI_VERSION}_amd64.deb"
fi

msg "Installing Dependencies"
apt-get install -y curl nginx gnupg lsb-release ca-certificates jq

msg "Installing Docker"
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
 apt-get update
 apt-get -y install docker-ce docker-ce-cli containerd.io

msg "Installing Docker Compose"
curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

echo 'KEYCLOAK_DB_PASS=$KEYCLOAK_DB_PASS' > .env
msg "Starting Keycloak"
docker-compose up -d

msg_dbg "Setting Keycloak Password"

docker exec local_keycloak \
    /opt/jboss/keycloak/bin/add-user-keycloak.sh \
    -u $KEYCLOAK_USER \
    -p $KEYCLOAK_PASS \
    
docker restart local_keycloak

cat <<EOF > realm.json
{
   "realm":"$KEYCLOAK_REALM",
   "notBefore":0,
   "enabled":true,
   "sslRequired":"all",
   "bruteForceProtected":true,
   "failureFactor":10,
   "eventsEnabled":false
}
EOF

msg "Installing Step CLI"
msg_dbg "Downloading Step CLI"
curl -sLO $STEPCLI_DOWNLOAD
msg_dbg "Installing Step CLI"
dpkg -i $(echo $STEPCLI_DOWNLOAD | grep -o "[A-Za-z0-9._-]*.deb")

msg_dbg "Creating Step Directories"
# All your CA config and certificates will go into $STEPPATH.
export STEPPATH=/etc/step-ca
mkdir -p $STEPPATH

# Configure `step` to connect to & trust our `step-ca`.
# Pull down the CA's root certificate so we can talk to it later with TLS
msg "Configuring CA root certifcates"
msg_dbg "Downloading CA's root certificate"
step ca bootstrap --ca-url $KEYCLOAK_CA_SERVER \
                  --fingerprint $KEYCLOAK_CA_FINGERPINT
# Install the CA cert for validating user certificates (from /etc/step-ca/certs/ssh_user_key.pub` on the CA).
msg_dbg "Installing CA root certificate"
step ssh config --roots > $(step path)/certs/ssh_user_key.pub
mkdir -p /usr/local/share/ca-certificates/extra
cp $STEPPATH/certs/root_ca.crt /usr/local/share/ca-certificates/extra/
update-ca-certificates

msg "Configuring NGNIX"
mkdir -p /var/www/le_root/.well-known/acme-challenge
chown -R root:www-data /var/www/le_root
mkdir -p /etc/nginx/includes
cp letsencrypt-webroot.tmpl /etc/nginx/includes/letsencrypt-webroot
rm -f /etc/nginx/sites-enabled/default
cp  lets_encrypt.tmpl /etc/nginx/sites-enabled/lets_encrypt
systemctl reload nginx
msg_dbg "Generating Certificate"
step ca certificate --provisioner ACME --webroot /var/www/le_root ${KEYCLOAK_DOMAIN} /etc/ssl/certs/keycloak.crt /etc/ssl/private/keycloak.key
msg_dbg "Loading Certificate"
cp keycloak.tmpl /etc/nginx/sites-enabled/keycloak
systemctl reload nginx

msg "Adding Certificate Renewal Service"
cp step.service /etc/systemd/system/
systemctl start step
systemctl enable step

msg "Waiting for Keycloak to start..."
sleep 30s

msg "Configuring Keycloak"
msg_dbg "Authenticating"
KEYCLOAK_ACCESS_TOKEN=$(curl \
  -X POST \
  -H "Content-Type: application/x-www-form-urlencoded" \
  -d "username=${KEYCLOAK_USER}" \
  -d "password=${KEYCLOAK_PASS}" \
  -d "grant_type=password" \
  -d 'client_id=admin-cli' \
  "127.0.0.1:8080/auth/realms/master/protocol/openid-connect/token"|jq -r '.access_token')

msg_dbg "Creating Keycloak Realm"
curl \
   -X POST \
   -H "Authorization: Bearer ${KEYCLOAK_ACCESS_TOKEN}" \
   -H "Content-Type: application/json" \
   -d @"realm.json" \
   "127.0.0.1:8080/auth/admin/realms";

msg_dbg "Creating ODIC Client"
curl \
   -X POST \
   -H "Authorization: Bearer ${KEYCLOAK_ACCESS_TOKEN}" \
   -H "Content-Type: application/json" \
   -d @"step-ca.json" \
   "127.0.0.1:8080/auth/admin/realms/$KEYCLOAK_REALM/clients";

msg_dbg "Getting Client Secret"
KEYCLOAK_CLIENTS=$(curl \
    -X GET \
    -H "Authorization: Bearer ${KEYCLOAK_ACCESS_TOKEN}" \
    -H "Content-Type: application/json" \
    "127.0.0.1:8080/auth/admin/realms/$KEYCLOAK_REALM/clients")

CLIENT_ID=$(echo $KEYCLOAK_CLIENTS | jq -r '.[] | "\(.clientId) \(.id)"' | grep step-ca | cut -d " " -f2)

KEYCLOAK_CLIENT_SECRET=$(curl \
    -X GET \
    -H "Authorization: Bearer ${KEYCLOAK_ACCESS_TOKEN}" \
    -H "Content-Type: application/json" \
    "127.0.0.1:8080/auth/admin/realms/$KEYCLOAK_REALM/clients/$CLIENT_ID/client-secret" | jq -r '.value')

msg "Client Secret: ${KEYCLOAK_CLIENT_SECRET}"



