#!/bin/bash
#
# This script will launch and configure a step-ca SSH Certificate Authority
# with OIDC
#
#
#
# See https://smallstep.com/blog/diy-single-sign-on-for-ssh/ for full instructions

set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd -P)

usage() {
  cat<<EOF
Usage: $(basename "${BASH_SOURCE[0]}")

Small Step CA initialization script

Available options:

-h, --help          Print this help and exit
-v, --verbose       Print script debug info
-n, --name          CA name
-d, --domain        CA domain names
--step-ca           Step CA version
--step-cli          Step CLI version
-s, --ssh           Enable SSH
-c, --create        Create Random Keys
-r, --root          Root Certificate Password
-i, --intermediate  Intermdiate Certificate Password
-h, --host          Host JWK Password
-u, --user          User JWK Password
-e, --email         User email for User JWK
EOF
  exit
}

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  # script cleanup here
}

setup_colors() {
  if [[ -t 2 ]] && [[ -z "${NO_COLOR-}" ]] && [[ "${TERM-}" != "dumb" ]]; then
    NOFORMAT='\033[0m' RED='\033[0;31m' GREEN='\033[0;32m' ORANGE='\033[0;33m' BLUE='\033[0;34m' PURPLE='\033[0;35m' CYAN='\033[0;36m' YELLOW='\033[1;33m'
  else
    NOFORMAT='' RED='' GREEN='' ORANGE='' BLUE='' PURPLE='' CYAN='' YELLOW=''
  fi
}

msg() {
  echo >&2 -e "${1-}"
}

msg_dbg() {
  if [[ "$verbose" == 1 ]];
  then
    echo >&2 -e "${1-}"
  fi
}

create_keys() {
   if [[ -z "$STEP_ROOT_PASS" ]] ;
   then
     STEP_ROOT_PASS=$(head -c 16  /dev/random | md5sum | cut -f 1 -d\ );
   fi
   if [[ -z "$STEP_INT_PASS" ]] ;
   then
     STEP_INT_PASS=$(head -c 16  /dev/random | md5sum | cut -f 1 -d\ );
   fi
   if [[ -z "$STEP_HOST_PASS" ]] ;
   then
     STEP_HOST_PASS=$(head -c 16  /dev/random | md5sum | cut -f 1 -d\ );
   fi
   if [[ -z "$STEP_USER_PASS" ]] ;
   then
     STEP_USER_PASS=$(head -c 16  /dev/random | md5sum | cut -f 1 -d\ );
   fi
}

parse_params() {
  # default values of variables set from params
  ssh=0
  create=0
  STEP_CA_NAME=""
  STEP_DOMAINS=""
  STEP_EMAIL=""
  STEP_ROOT_PASS=""
  STEP_INT_PASS=""
  STEP_HOST_PASS=""
  STEP_USER_PASS=""
  STEP_CLI_VERSION=""
  STEP_CA_VERSION=""
  verbose=0
  while :; do
    case "${1-}" in
    -h | --help) usage ;;
    -v | --verbose) verbose=1 ;;
    -c | --create) create_keys ;;
    -n | --name)
      STEP_CA_NAME="${2-}"
      shift
      ;;
    -d | --domains)
      STEP_DOMAINS="${2-}"
      shift
      ;;
    --step-cli)
      STEP_CLI_VERSION="${2-}"
      shift
      ;;
    --step-ca)
      STEP_CA_VERSION="${2-}"
      shift
      ;;
    -s | --ssh) ssh=1 ;;
    -r | --root)
      STEP_ROOT_PASS="${2-}"
      shift
      ;;
    -i | --intermediate)
      STEP_INT_PASS="${2-}"
      shift
      ;;
    -h | --host)
      STEP_HOST_PASS="${2-}"
      shift
      ;;
    -u | --user)
      STEP_USER_PASS="${2-}"
      shift
      ;;
    -e | --email)
      STEP_EMAIL="${2-}"
      shift
      ;;
    -?*) die "Unknown option: $1" ;;
    *) break ;;
    esac
    shift
  done

  args=("$@")

  # check required params and arguments
  #[[ -z "${param-}" ]] && die "Missing required parameter: param"
  #[[ ${#args[@]} -eq 0 ]] && die "Missing script arguments"

  return 0
}


parse_params "$@"

msg "Setting up Step CA"
msg_dbg "Configuring Parameters"
if [[ -z "$STEP_CA_NAME" ]];
then
  msg "CA Name: "
  read STEP_CA_NAME
fi
if [[  -z "$STEP_ROOT_PASS" ]];
then
  msg "Enter Root Key Password: "
  read -s STEP_ROOT_PASS
  msg "Retype Root Key Password: "
  read -s STEP_ROOT_PASS1
  msg ""
  while [ $STEP_ROOT_PASS != $STEP_ROOT_PASS1 ];
  do
    msg "Root Key password does not match\n"
    msg "Enter Root Key Password: "
    read -s STEP_ROOT_PASS
    msg "Retype Root Key Password: "
    read -s STEP_ROOT_PASS1
    msg ""
  done
fi
if [[  -z "$STEP_INT_PASS" ]];
then
  msg "Enter Intermediate Key Password: "
  read -s STEP_INT_PASS
  msg "Retype Intermediate Key Password: "
  read -s STEP_INT_PASS1
  msg ""
  while [ $STEP_INT_PASS != $STEP_INT_PASS1 ];
  do
    msg "Intermediate Key password does not match\n"
    msg "Enter Intermediate Key Password: "
    read -s STEP_INT_PASS
    msg "Retype Root Key Password: "
    read -s STEP_INT_PASS1
    msg ""
  done
fi
if [[ -z "$STEP_USER_PASS" ]];
then
  msg "Enter User JWK Password: "
  read -s STEP_USER_PASS
  msg "Retype User JWK Password: "
  read -s STEP_USER_PASS2
  msg ""
  while [ $STEP_USER_PASS != $STEP_USER_PASS2 ];
  do
    msg "User JWK password does not match"
    msg "Enter User JWK Password: "
    read -s STEP_USER_PASS
    msg "Retype User JWK Password: "
    read -s STEP_USER_PASS2
    msg ""
  done
fi
if [[ -z "$STEP_HOST_PASS" && $ssh == 1 ]];
then
  msg "Enter Host JWK Password: "
  read -s STEP_HOST_PASS
  msg "Retype Host JWK Password: "
  read -s STEP_HOST_PASS2
  msg ""
  while [ $STEP_HOST_PASS != $STEP_HOST_PASS2 ];
  do
    msg "Host JWK password does not match"
    msg "Enter Host JWK Password: "
    read -s STEP_HOST_PASS
    msg "Retype Host JWK Password: "
    read -s STEP_HOST_PASS2
    msg ""
  done
fi
if [[ -z "$STEP_EMAIL" ]];
then
  msg "Enter User Email: "
  read STEP_EMAIL
fi
if [[ -z "$STEP_DOMAINS" ]];
then
  msg "Enter CA domain names (comma separated): "
  read STEP_DOMAINS
fi

if [[ -z "$STEP_CA_VERSION" ]]
then
    STEPCA_DOWNLOAD=$(curl -s https://api.github.com/repos/smallstep/certificates/releases/latest | grep "browser_download_url.*deb" | cut -d : -f 2,3 | tr -d \")
else
    STEPCA_DOWNLOAD="https://github.com/smallstep/certificates/releases/download/v${STEPCA_VERSION}/step-ca_${STEPCLI_VERSION}_amd64.deb"
fi
if [[ -z "$STEP_CLI_VERSION" ]]
then
    STEPCLI_DOWNLOAD=$(curl -s https://api.github.com/repos/smallstep/cli/releases/latest | grep "browser_download_url.*deb" | cut -d : -f 2,3 | tr -d \")
else
    STEPCLI_DOWNLOAD="https://github.com/smallstep/cli/releases/download/v${STEPCLI_VERSION}/step-cli_${STEPCLI_VERSION}_amd64.deb"
fi

PRIMARY_IP=$(ip route get 1 | sed -n 's/^.*src \([0-9.]*\) .*$/\1/p')

msg "Installing Step"
msg_dbg "Downloading Step-CA"
curl -sLO $STEPCA_DOWNLOAD
msg_dbg "Downloading Step-CLI"
curl -sLO $STEPCLI_DOWNLOAD
msg_dbg "Installing Step CA"
dpkg -i $(echo $STEPCA_DOWNLOAD | grep -o "[A-Za-z0-9._-]*.deb")
msg_dbg "Installing Step CLI"
dpkg -i $(echo $STEPCLI_DOWNLOAD | grep -o "[A-Za-z0-9._-]*.deb")

msg_dbg "Creating Step Directories"
# All your CA config and certificates will go into $STEPPATH.
export STEPPATH=/etc/step-ca
mkdir -p $STEPPATH/db
msg_dbg "Writing Password files"
echo $STEP_ROOT_PASS > $STEPPATH/root_pass.txt
echo $STEP_INT_PASS > $STEPPATH/int_pass.txt
echo $STEP_USER_PASS > $STEPPATH/user_pass.txt
if [[ $ssh == 1 ]];
then
  echo $STEP_HOST_PASS > $STEPPATH/host_pass.txt
fi

msg_dbg "Adding CA service user"
useradd --system --home /etc/step-ca --shell /bin/false step
setcap CAP_NET_BIND_SERVICE=+eip $(which step-ca)

msg_dbg "Setting directory permisions"
chown -R step:step $STEPPATH
chmod 700 $STEPPATH
msg_dbg "Adding to Systemd"
# https://smallstep.com/docs/step-ca/certificate-authority-server-production
# Add a service to systemd for our CA.
cat <<'EOF' > /etc/systemd/system/step-ca.service
[Unit]
Description=step-ca service
Documentation=https://smallstep.com/docs/step-ca
Documentation=https://smallstep.com/docs/step-ca/certificate-authority-server-production
After=network-online.target
Wants=network-online.target
StartLimitIntervalSec=30
StartLimitBurst=3
ConditionFileNotEmpty=/etc/step-ca/config/ca.json
ConditionFileNotEmpty=/etc/step-ca/int_pass.txt

[Service]
Type=simple
User=step
Group=step
Environment=STEPPATH=/etc/step-ca
WorkingDirectory=/etc/step-ca
ExecStart=/usr/bin/step-ca config/ca.json --password-file int_pass.txt
ExecReload=/bin/kill --signal HUP $MAINPID
Restart=on-failure
RestartSec=5
TimeoutStopSec=30
StartLimitInterval=30
StartLimitBurst=3

; Process capabilities & privileges
AmbientCapabilities=CAP_NET_BIND_SERVICE
CapabilityBoundingSet=CAP_NET_BIND_SERVICE
SecureBits=keep-caps
NoNewPrivileges=yes

; Sandboxing
ProtectSystem=full
ProtectHome=true
RestrictNamespaces=true
RestrictAddressFamilies=AF_UNIX AF_INET AF_INET6
PrivateTmp=true
PrivateDevices=true
ProtectClock=true
ProtectControlGroups=true
ProtectKernelTunables=true
ProtectKernelLogs=true
ProtectKernelModules=true
LockPersonality=true
RestrictSUIDSGID=true
RemoveIPC=true
RestrictRealtime=true
SystemCallFilter=@system-service
SystemCallArchitectures=native
MemoryDenyWriteExecute=true
ReadWriteDirectories=/etc/step-ca/db

[Install]
WantedBy=multi-user.target
EOF

msg "Creating CA"

msg_dbg "Initializing CA"
if [[  $ssh == 1 ]];
then
 # Set up our basic CA configuration and generate root keys
 step ca init --ssh --name="$STEP_CA_NAME" \
     --dns="$PRIMARY_IP,$STEP_DOMAINS" \
     --address=":443" --provisioner="temp" \
     --password-file="$STEPPATH/root_pass.txt"
else
# Set up our basic CA configuration and generate root keys
 step ca init --name="$STEP_CA_NAME" \
      --dns="$PRIMARY_IP,$STEP_DOMAINS" \
      --address=":443" --provisioner="temp" \
      --password-file="$STEPPATH/root_pass.txt"
fi

msg_dbg "Updating Certificate Passwords"
step crypto change-pass "$STEPPATH/secrets/intermediate_ca_key" --password-file="$STEPPATH/root_pass.txt" --new-password-file="$STEPPATH/int_pass.txt" -f
step crypto change-pass "$STEPPATH/secrets/ssh_user_ca_key" --password-file="$STEPPATH/root_pass.txt" --new-password-file="$STEPPATH/int_pass.txt" -f
if [[ $ssh == 1 ]];
then
  step crypto change-pass "$STEPPATH/secrets/ssh_host_ca_key" --password-file="$STEPPATH/root_pass.txt" --new-password-file="$STEPPATH/int_pass.txt" -f
fi


chown -R step:step $STEPPATH

msg_dbg "Adding User JWK Provisioner"
step ca provisioner add $STEP_EMAIL --type="JWK" --create --ssh --password-file="$STEPPATH/user_pass.txt"

msg_dbg "Adding ACME Provisioner"
step ca provisioner add ACME --type="ACME"

msg_dbg "Removing Default Provisioner"
step ca provisioner remove temp  --ca-config=$STEPPATH/config/ca.json --all


if [[ $ssh == 1 ]];
then
  msg_dbg "Adding Host JWK Provisioner"
  step ca provisioner add Host_JWK --type="JWK" --create --ssh --password-file="$STEPPATH/host_pass.txt"
fi

systemctl daemon-reload
systemctl enable --now step-ca
