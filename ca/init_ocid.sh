#!/bin/bash
#
# This script will configure Step CA with an ODIC
# 
# 

set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd -P)

usage() {
  cat<<EOF
Usage: $(basename "${BASH_SOURCE[0]}")

Step CA ODIC initialization script

Available options:

-h, --help          Print this help and exit
-v, --verbose       Print script debug info
-o, --odic          ODIC Sever
-c, --client        ODIC Client
-s, --secret        ODIC Client Secret
-e, --endpoint      ODIC Endpoint
-n, --name          Provisioner Name
-d, --default       Set Provisioner as Default SSH login
EOF
  exit
}

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  # script cleanup here
}

setup_colors() {
  if [[ -t 2 ]] && [[ -z "${NO_COLOR-}" ]] && [[ "${TERM-}" != "dumb" ]]; then
    NOFORMAT='\033[0m' RED='\033[0;31m' GREEN='\033[0;32m' ORANGE='\033[0;33m' BLUE='\033[0;34m' PURPLE='\033[0;35m' CYAN='\033[0;36m' YELLOW='\033[1;33m'
  else
    NOFORMAT='' RED='' GREEN='' ORANGE='' BLUE='' PURPLE='' CYAN='' YELLOW=''
  fi
}

msg() {
  echo >&2 -e "${1-}"
}

msg_dbg() {
  if [[ "$verbose" == 1 ]];
  then
    echo >&2 -e "${1-}"
  fi
}


parse_params() {
  # default values of variables set from params
  ODIC_SERVER=""
  ODIC_CLIENT=""
  ODIC_SECRET=""
  ODIC_ENDPOINT=""
  ODIC_NAME=""
  verbose=0
  default=0
  while :; do
    case "${1-}" in
    -h | --help) usage ;;
    -v | --verbose) verbose=1 ;;
    -d | --default) verbose=1 ;;
    -o | --odic)
      ODIC_SERVER="${2-}"
      shift
      ;;
    -c | --client)
      ODIC_CLIENT="${2-}"
      shift
      ;;
    -s | --secret)
      ODIC_SECRET="${2-}"
      shift
      ;;
    -e | --endpoint)
      ODIC_ENDPOINT="${2-}"
      shift
      ;;
    -n | --name)
      ODIC_NAME="${2-}"
      shift
      ;;
    -?*) die "Unknown option: $1" ;;
    *) break ;;
    esac
    shift
  done

  args=("$@")

  # check required params and arguments
  #[[ -z "${param-}" ]] && die "Missing required parameter: param"
  #[[ ${#args[@]} -eq 0 ]] && die "Missing script arguments"

  return 0
}

arse_params "$@"

if [[ -z "$ODIC_SERVER" ]];
then
  msg "ODIC Server (keycloak.local): "
  read ODIC_SERVER
fi
if [[ -z "$ODIC_CLIENT" ]];
then
  msg "ODIC Client Name: "
  read ODIC_CLIENT
fi
if [[  -z "$ODIC_SECRET" ]];
then
  msg "Enter ODIC Client Secret: "
  read -s ODIC_SECRET
  msg "Retype ODIC Client Secret: "
  read -s ODIC_SECRET1
  msg ""
  while [ $ODIC_SECRET != $ODIC_SECRET1 ];
  do
    msg "ODIC Client Secret does not match\n"
    msg "Enter ODIC Client Secret: "
    read -s ODIC_SECRET
    msg "Retype ODIC Client Secret: "
    read -s ODIC_SECRET1
    msg ""
  done
fi
if [[ -z "$ODIC_ENDPOINT" ]];
then
  msg "ODIC Endpoint: "
  read ODIC_ENDPOINT
fi
if [[ -z "$ODIC_NAME" ]];
then
  msg "ODIC Provisioner Name : "
  read ODIC_NAME
fi

step ca provisioner add ODIC_NAME --type OIDC --ssh \
                       --client-id ODIC_CLIENT --client-secret $ODIC_SECRET \
                       --configuration-endpoint $ODIC_ENDPOINT \
                       --listen-address :10000
